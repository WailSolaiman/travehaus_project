<header class="banner">
  <div class="container">
    <!--<a class="brand" href="<#?= esc_url(home_url('/')); ?>"></a>-->
    <div class="brand">
      <a href="<?php echo home_url(); ?>">
        <h1>
          <?= get_bloginfo('name') ?>
        </h1>
      </a>
      <p>
        <?= get_bloginfo('description') ?>
      </p>
    </div>
    <div class="lang">
      <p class="de">DE</p>
      <p class="char"> | </p>
      <p class="en">EN</p>
    </div>
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>




    <nav class="nav-mobile">
      <div id="toggle" class="button_container">
        <span class="top"></span>
        <span class="middle"></span>
        <span class="bottom"></span>
      </div>
      <div id="overlay" class="overlay">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
          endif;
        ?>
      </div>
    </nav>




  </div>
</header>


<div class="container-fluid">
  <div class="page-header">
    <?php if ( has_post_thumbnail() ) {
    the_post_thumbnail('featured-image', ['title' => 'Feature image']);
  }  else {
    echo '<img src="https://placeholdit.imgix.net/~text?txtsize=16&txt=noch+kein+Bild&w=1110&h=500" class="img-responsive full-bleed">';
  } ?>
  </div>
</div>
