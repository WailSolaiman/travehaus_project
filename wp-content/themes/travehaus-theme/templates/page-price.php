<?php
/*
Template Name: Price Template
*/
?>
  <div class="wp-page">
    <div class="container">
      <?php while (have_posts()) : the_post(); ?>
      <article <?php post_class(); ?>>
        <header>
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
        <?php comments_template('/templates/comments.php'); ?>
      </article>
      <?php endwhile; ?>
    </div>
  </div>

  <div class="container-fluid" style="position: relative; overflow: hidden;">
    <div class="background-image"></div>
    <div class="full-bleed prices">
      <div class="container">
        <div class="row">
          <div class="col-md-2"></div>
          <p> </p>
          <div class="col-md-4 price">
            <div class="content-wrapper">
              <h2>Hauptsaison</h2>
              <p class="date">15. Mai bis 14. Sept.</p>
              <p class="date">15. Mai bis 14. Sept.</p>
              <hr>
              <p class="personen">1 bis 2 Personen</p>
              <h3 class="single-price">130 Euro</h3>
              <hr>
              <p class="personen">3 bis 4 Personen</p>
              <h3 class="single-price">140 Euro</h3>
              <p class="last">pro Übernachtung</p>
            </div>
          </div>
          <div class="col-md-4 price">
            <div class="content-wrapper">
              <h2>Hauptsaison</h2>
              <p class="date">15. Mai bis 14. Sept.</p>
              <p class="date">15. Mai bis 14. Sept.</p>
              <hr>
              <p class="personen">1 bis 2 Personen</p>
              <h3 class="single-price">130 Euro</h3>
              <hr>
              <p class="personen">3 bis 4 Personen</p>
              <h3 class="single-price">140 Euro</h3>
              <p class="last">pro Übernachtung</p>
            </div>
          </div>
          <p></p>
          <div class="col-md-2"></div>
        </div>
      </div>
    </div>
  </div>
