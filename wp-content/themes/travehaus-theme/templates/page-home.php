<?php
/*
Template Name: Startpage Template
*/
?>
  <div class="wp-page">
    <div class="container">
      <?php while (have_posts()) : the_post(); ?>
      <article <?php post_class(); ?>>
        <header>
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
        <footer>
          <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
        </footer>
        <?php comments_template('/templates/comments.php'); ?>
      </article>
      <?php endwhile; ?>
    </div>

    <div class="container-fluid">
      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Image Widget')): ?>
      <?php endif;?>
    </div>
  </div>
