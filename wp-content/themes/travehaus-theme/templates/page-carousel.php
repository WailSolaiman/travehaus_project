<?php
/*
Template Name: Carousel Template
*/
?>
  <div class="wp-page">
    <div class="container">
      <?php while (have_posts()) : the_post(); ?>
      <article <?php post_class(); ?>>
        <header>
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
        <?php comments_template('/templates/comments.php'); ?>
      </article>
      <?php endwhile; ?>
    </div>

    <div class="container carousel">
      <h1 class="carousel-header">Galerie</h1>
      <div class="main-carousel">
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/1.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/2.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/3.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/4.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/5.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/6.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/7.png">
        </div>
      </div>
      <div class="nav-carousel">
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/1.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/2.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/3.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/4.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/5.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/6.png">
        </div>
        <div class="carousel-cell">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/7.png">
        </div>
      </div>
    </div>
  </div>
