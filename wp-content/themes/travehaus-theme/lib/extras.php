<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

// Add Metaboxs
add_filter('rwmb_meta_boxes', __NAMESPACE__.'\\posts_meta_boxes');
function posts_meta_boxes($meta_boxes)
{
  $meta_boxes[] = array(
    'title' => __('Zusätzliche Felder', 'travehaus-theme'),
    'post_types' => array(
      'page'
    ),
    'fields' => array(

      array(
        'id' => 'slider_gallery',
        'name' => __('Bilder', 'travehaus-theme'),
      
        'type' => 'image_advanced',
        // Delete image from Media Library when remove it from post meta?
        // Note: it might affect other posts if you use same image for multiple posts
        'force_delete' => false,
        'clone' => false,
        'multiple' => true,

      )
    ),
  );

  return $meta_boxes;
}
